# EP2 - Orientação à objetos (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a tecnologia Java Swing.

# Como iniciar
O software pode ser inicializado fazendo-se o download do diretório, abrindo o projeto na IDE Eclipse e executando-o como uma aplicação java.

# Restrições e observações
	-Para o cadastro de usuários deve-se utilizar a senha de administrador "12345678" ou "87654321";
	
	-Os campos de nomes (de funcionários, produtos e clientes) podem conter no máximo 30 caracteres;
	
	-Ao modificar um produto, os campos deixados em branco não serão alterados;
	
	-Os arquivos padrão do programa são salvos no diretório "EP2/To com fome, quero mais/defaults" e, para que o programa funcione corretamente, eles não podem ser alterados, porém, podem ser excluídos, pois serão criados novamente;
	
	-Quando o estoque de um produto ficar baixo, ele só aparecerá na lista para ser vendido novamento caso sua quantidade seja aumentada através do menu "Modificar Produto"
	
# IDE usada na maior parte desenvolvimento
IDE Eclipse Version: Neon.1a Release (4.6.1) e java 8

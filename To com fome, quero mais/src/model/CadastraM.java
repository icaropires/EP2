package model;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import java.io.File;


public abstract class CadastraM {
	protected File file;
	protected FileWriter filew;
	protected PrintWriter writer;
	
	
	public CadastraM(){
	}
	
	//Os arquivos sempre serão postos na pasta defaults
	public static void criarArquivo(String titulo, String nome){
		File file;
		FileWriter filew;
		PrintWriter writer;
		String endereco = "./defaults/" + nome;
		
		try{
		file = new File(endereco);
		
			if(!file.exists()) {
				file.createNewFile();
				filew = new FileWriter(endereco, true);
				writer = new PrintWriter(filew);
				
				writer.println(titulo + "\n");
				
				filew.close();
			}
		}
		catch(IOException e){
			System.out.println("Não foi possível criar o arquivo de título: " + nome);
		}
	}
	public interface Cadastrar{
		public void cadastra();
	}
}

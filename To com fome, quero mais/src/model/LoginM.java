package model;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class LoginM {
	private ArrayList<String> funcionarios = new ArrayList<String>();
	private ArrayList<String> senhas = new ArrayList<String>();
	
	private String nomeAvaliado;
	private String senhaAvaliada;
	
	public LoginM(){
		leiaArquivo();
	}
	
	public LoginM(String nome, char[] senhaChar){
		nomeAvaliado = nome; //guardar o valor que será avaliado
		senhaAvaliada = String.valueOf(senhaChar); //aqui também
		
		leiaArquivo();
	}
	
	public boolean valida(){
		
		if(funcionarios.contains(nomeAvaliado) && 
				senhas.get(funcionarios.indexOf(nomeAvaliado)).equals(senhaAvaliada)){
			
			return true;
		}
		
		return false;
	}
	
	public void leiaArquivo(){
		try{
			CadastraM.criarArquivo("#FUNCIONARIOS CADASTRADOS", "funcionarios");
			
			FileReader fileReader = new FileReader("./defaults/funcionarios");
			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(fileReader);
			
			/*pular cabeçalho*/
			scanner.nextLine();
			scanner.nextLine();
			/*               */
			
			while(scanner.hasNext()){
				funcionarios.add(scanner.nextLine());
				senhas.add(scanner.nextLine());
			}
		}
		catch(IOException e){
			System.out.println("Não foi possível ler o arquivo funcionário");
		}
	}
	public ArrayList<String> getFuncionarios() {
		return funcionarios;
	}

	public ArrayList<String> getSenhas() {
		return senhas;
	}
}

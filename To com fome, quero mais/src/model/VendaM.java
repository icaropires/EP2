package model;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import controller.CadastrarPedidoC;
import model.CadastraM.Cadastrar;
import view.SelecionarProdutoV;

public class VendaM extends CadastraM implements Cadastrar{
	private String vendedor;
	private JFormattedTextField mesa;
	private ArrayList<SelecionarProdutoV> produtos;
	private JFormattedTextField cliente;
	private JComboBox<String> formaDePagamento;
	
	public VendaM(CadastrarPedidoC pedido){
		vendedor = pedido.getVendedor();
		produtos = pedido.getMenusProdutos();
		mesa = pedido.getMesa();
		formaDePagamento = pedido.getFormaDePagamento();
		cliente = pedido.getNome();
	}
	
	public void cadastra(){
		
		criarArquivo("#VENDAS REALIZADAS", "vendas");
		
		try{
			filew = new FileWriter("./defaults/vendas", true);
			writer = new PrintWriter(filew);
		
			String dia = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
			String hora = new SimpleDateFormat("HH:mm:ss").format(new Date());
			
			writer.println(dia);
			writer.println(hora);
			
			writer.println(vendedor);
			
			writer.println("@");
			for(int i = 0; i < produtos.size(); i++){
				writer.println(produtos.get(i).getProdutoBox().getSelectedItem().toString());
				
				writer.println(produtos.get(i).getQuantidadeBox().getSelectedItem().toString());
				
				if(produtos.get(i).getObservacoes().getText().equals(""))
					writer.println("Não houve");
				else
					writer.println(produtos.get(i).getObservacoes().getText());
			}
			writer.println("@");
			
			writer.println(cliente.getValue().toString());
			writer.println(mesa.getValue().toString());
			writer.println(formaDePagamento.getSelectedItem().toString());
			
			filew.close();
		}
		catch(IOException e){
			System.out.println("Não foi possível criar o arquivo para registro de vendas");
		}
	
	}
}

package model;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JPasswordField;

import model.CadastraM.Cadastrar;

public class FuncionarioM extends CadastraM implements Cadastrar{
	private String nomeFuncionario;
	private char[] senhaFuncionario;
	
	public FuncionarioM(){};
	
	public FuncionarioM(String nomeFuncionario, char[] senhaFuncionario){
		this.nomeFuncionario = nomeFuncionario;
		this.senhaFuncionario = senhaFuncionario;
	}
	
	public void cadastra(){
		criarArquivo("#FUNCIONARIOS CADASTRADOS", "funcionarios");
		
		try{
			
			filew = new FileWriter("./defaults/funcionarios", true);
			writer = new PrintWriter(filew);
			
			writer.println(nomeFuncionario);
			writer.println(senhaFuncionario);
			filew.close();
		}
		catch(IOException e){
			System.out.println("Não foi possivel escrever no arquivo");
		}
	}
	
	public boolean validaSenhaAdm(JPasswordField senhaAdm){
		String senhaAdmString = String.valueOf(senhaAdm.getPassword());
		
		if(senhaAdmString.equals("12345678")){
			return true;
		}
		else if(senhaAdmString.equals("87654321")){
			return true;
		}
		
		return false;
	}
	
}

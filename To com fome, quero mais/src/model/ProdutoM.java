package model;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class ProdutoM extends CadastraM{
	//Para leitura
	private ArrayList<String> produtos = new ArrayList<String>();
	private ArrayList<Double> precos = new ArrayList<Double>();
	private ArrayList<Integer> quantidades = new ArrayList<Integer>();
	private ArrayList<Integer> quantidadesMin = new ArrayList<Integer>();
	
	//Para cadastrar
	private ArrayList<String> produtosNovos = new ArrayList<String>();
	private ArrayList<String> precosNovos = new ArrayList<String>();
	private ArrayList<String> quantidadesNovos = new ArrayList<String>();
	private ArrayList<String> quantidadesMinNovos = new ArrayList<String>();
	
	int novaQuantidade;
	int indice;
	
	public ProdutoM(){
		leiaArquivo();
		atualizaNovos();
	}
		
	public ProdutoM(String produto, String preco, String quantidade, String quantidadeMin){
		produtosNovos.add(produto);
		precosNovos.add(preco);
		quantidadesNovos.add(quantidade);
		quantidadesMinNovos.add(quantidadeMin);
		
		leiaArquivo();
	}
	
	public void cadastra(){
		
		criarArquivo("#PRODUTOS CADASTRADOS", "produtos");	
	
		try{
			
			filew = new FileWriter("./defaults/produtos", true);
			writer = new PrintWriter(filew);
			
			for(int i = 0; i < produtosNovos.size(); i++){
				writer.println(produtosNovos.get(i));
				
				if (!produtos.contains(produtosNovos.get(i))) //Produto está sendo cadastrado
					writer.println(Double.parseDouble(precosNovos.get(i))/100.0);
				else
					writer.println(precosNovos.get(i));
				
				writer.println(quantidadesNovos.get(i));
				writer.println(quantidadesMinNovos.get(i));
			}
			
			filew.close();
		}
		catch(IOException e){
			System.out.println("Não foi possivel escrever no arquivo");
		}
	}
	
	public void atualizaNovos(){
		produtosNovos.clear();
		precosNovos.clear();
		quantidadesNovos.clear();
		quantidadesMinNovos.clear();
		
		for(int i = 0; i < produtos.size(); i++){
			
			produtosNovos.add(produtos.get(i).toString());
			precosNovos.add(precos.get(i).toString());
			quantidadesNovos.add(quantidades.get(i).toString());
			quantidadesMinNovos.add(quantidadesMin.get(i).toString());
		}
	}
	
	public int descontarQuantidade(String produto, String quantidadeVendida){
		
		int indice = produtos.indexOf(produto);
		novaQuantidade = quantidades.get(indice) - Integer.parseInt(quantidadeVendida);
		
		quantidadesNovos.set(indice, Integer.toString(novaQuantidade));
		
		return novaQuantidade;
	}
	
	public void alterar(int indice, String novoNome, String novoPreco, String novaQuantidade, String novaQuantidadeMin){
		
		produtosNovos.set(indice, novoNome);
		precosNovos.set(indice, novoPreco);
		quantidadesNovos.set(indice, novaQuantidade);
		quantidadesMinNovos.set(indice, novaQuantidadeMin);
	}
	
	public void leiaArquivo(){
		produtos.clear();
		precos.clear();
		quantidades.clear();
		quantidadesMin.clear();
		
		try{
			
			criarArquivo("#PRODUTOS CADASTRADOS", "produtos");
			@SuppressWarnings("resource")
			Scanner scanner = new Scanner(new FileReader("./defaults/produtos"));
			scanner.useLocale(Locale.US);
		
			/*Pular cabeçalho*/
			scanner.nextLine();
			scanner.nextLine();
			/*				*/
		
			while(scanner.hasNext()){
				produtos.add(scanner.nextLine());
				precos.add(scanner.nextDouble());
				quantidades.add(scanner.nextInt());
				quantidadesMin.add(scanner.nextInt());
				scanner.nextLine(); //pular \n
			}
			
		}
		
		catch(IOException e){
			System.out.println("Não foi possível criar o arquivo funcionarios");
		}
	}
	
	public ArrayList<String> getProdutosNovos() {
		return produtosNovos;
	}

	public void setProdutosNovos(ArrayList<String> produtosNovos) {
		this.produtosNovos = produtosNovos;
	}

	public ArrayList<String> getPrecosNovos() {
		return precosNovos;
	}

	public void setPrecosNovos(ArrayList<String> precosNovos) {
		this.precosNovos = precosNovos;
	}

	public ArrayList<String> getQuantidadesNovos() {
		return quantidadesNovos;
	}

	public void setQuantidadesNovos(ArrayList<String> quantidadesNovos) {
		this.quantidadesNovos = quantidadesNovos;
	}

	public ArrayList<String> getQuantidadesMinNovos() {
		return quantidadesMinNovos;
	}

	public void setQuantidadesMinNovos(ArrayList<String> quantidadesMinNovos) {
		this.quantidadesMinNovos = quantidadesMinNovos;
	}
	
	public ArrayList<String> getProdutos() {
		return produtos;
	}
	
	public ArrayList<Integer> getQuantidades() {
		return quantidades;
	}
	
	public ArrayList<Integer> getQuantidadesMin() {
		return quantidadesMin;
	}
	public ArrayList<Double> getPrecos() {
		return precos;
	}

}

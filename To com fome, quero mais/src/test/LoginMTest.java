package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import model.LoginM;

public class LoginMTest {
	private ArrayList<String> funcionarios = new ArrayList<String>();
	private ArrayList<String> senhas = new ArrayList<String>();
	
	private String nomeAvaliado;
	private char[] senhaAvaliada;
	
	@Test
	public void validaTest() {
		funcionarios.add("Exemplo1");
		senhas.add("87654321");
		funcionarios.add("Exemplo2");
		senhas.add("123456");
		
		LoginM login;
		
		nomeAvaliado = "abcdefghijklmnopq";
		senhaAvaliada = "iweurhiueh".toCharArray();
		
		login = new LoginM(nomeAvaliado, senhaAvaliada);
		
		assertEquals(false, login.valida());
		
		nomeAvaliado = "2361238712683217";
		senhaAvaliada = "dsdsafsdfdssadf".toCharArray();
		
		login = new LoginM(nomeAvaliado, senhaAvaliada);
		
		assertEquals(false, login.valida());
		
		nomeAvaliado = "xxxxxxxxxxxxx";
		senhaAvaliada = "xxxxxxxxxxxx".toCharArray();
		
		login = new LoginM(nomeAvaliado, senhaAvaliada);
		
		assertEquals(false, login.valida());
		
		nomeAvaliado = "Exemplo1";
		senhaAvaliada = "87654321".toCharArray();
		
		login = new LoginM(nomeAvaliado, senhaAvaliada);
		
		assertEquals(false, login.valida());

		
		nomeAvaliado = "Exemplo2";
		senhaAvaliada = "123456".toCharArray();
		
		login = new LoginM(nomeAvaliado, senhaAvaliada);
		
		assertEquals(false, login.valida());
		
	}

}

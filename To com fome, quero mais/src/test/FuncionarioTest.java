package test;

import static org.junit.Assert.*;

import javax.swing.JPasswordField;

import org.junit.Test;

import model.FuncionarioM;

public class FuncionarioTest {
	JPasswordField senha = new JPasswordField();
	FuncionarioM funcionario = new FuncionarioM();
	
	@Test
	public void testValidaSenhaAdm() {
		senha.setText("15681689");
		assertEquals(false, funcionario.validaSenhaAdm(senha));
		
		senha.setText("abcde");
		assertEquals(false, funcionario.validaSenhaAdm(senha));
		
		senha.setText("00000000");
		assertEquals(false, funcionario.validaSenhaAdm(senha));
		
		senha.setText("!@#$%*()");
		assertEquals(false, funcionario.validaSenhaAdm(senha));
		
		senha.setText("");
		assertEquals(false, funcionario.validaSenhaAdm(senha));
		
		senha.setText("12345678");
		assertEquals(true, funcionario.validaSenhaAdm(senha));
		
		senha.setText("87654321");
		assertEquals(true, funcionario.validaSenhaAdm(senha));
		
	}

}

package test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.ProdutoM;

public class ProdutoMTest {
	ProdutoM produto = new ProdutoM();

	@Test
	public void atualizaNovosTest() {
		
		produto.atualizaNovos();
		
		for(int i = 0; i < produto.getProdutos().size(); i++){
			
			assertEquals(produto.getProdutosNovos().get(i), produto.getProdutos().get(i));
			assertEquals(produto.getPrecosNovos().get(i), produto.getPrecos().get(i).toString());
			assertEquals(produto.getQuantidadesNovos().get(i), produto.getQuantidades().get(i).toString());
			assertEquals(produto.getQuantidadesMinNovos().get(i), produto.getQuantidadesMin().get(i).toString());
		}
	}
	
	@Test
	public void descontarQuantidadeTest() {
		produto.getProdutos().set(0, "Pão");
		produto.getQuantidades().set(0, 10);
		
		assertEquals(8, produto.descontarQuantidade(produto.getProdutos().get(0), "2"));
	}

	@Test
	public void alterarTest(){
		produto.alterar(0, "nome novo", "1.99", "70", "2");
		
		assertEquals(produto.getProdutosNovos().get(0), "nome novo");
		assertEquals(produto.getQuantidadesNovos().get(0), "70");
		assertEquals(produto.getPrecosNovos().get(0), "1.99");
		assertEquals(produto.getQuantidadesMinNovos().get(0), "2");
	}
}

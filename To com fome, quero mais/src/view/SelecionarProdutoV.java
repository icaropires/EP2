package view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.SelecionarProdutoC;
import model.ProdutoM;
import view.LoginAndCadastrosV.ContainersInternosV;

public class SelecionarProdutoV implements ContainersInternosV{
	//Containers
	private JPanel bgPrincipal;
	private JPanel produtoBg;
	private JPanel quantidadeBg;
	private JPanel observacoesBg;
	private JPanel infoBg;
	
	//Combo Box
	private JComboBox<String> produtoBox;
	private JComboBox<Integer> quantidadeBox;
	
	//Labels
	private JLabel produtoLabel;
	private JLabel quantidadeLabel;
	private JLabel quantidadeMaxLabel;
	private JLabel precoInfoLabel;
	private JLabel quantidadeMinLabel;
	private JLabel observacoesLabel;
	
	//Limites
	private GridBagConstraints produtoLabelLim;
	private GridBagConstraints quantidadeLabelLim;
	private GridBagConstraints produtoBoxLim;
	private GridBagConstraints quantidadeBoxLim;
	private GridBagConstraints observacoesLim;
	
	//Medidas
	private int larguraBgPrincipal;
	private int alturaBgPrincipal;
	private int larguraBgQuantidadeMax;
	
	//campos de texto
	private JFormattedTextField observacoes;
	
	//listener
	private SelecionarProdutoC.ComboBoxListener produtoListener; //listener combox
	
	//
	private ProdutoM produtosCadastrados;
	
	//
	private boolean semEstoque;
	
	public SelecionarProdutoV() {
		produtosCadastrados = new ProdutoM();
		produtoBox = new JComboBox<String>();
		
		//Configurar Containers
		larguraBgPrincipal = 800;
		alturaBgPrincipal = 70;
		larguraBgQuantidadeMax = 150;
		
		bgPrincipal = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		bgPrincipal.setPreferredSize(new Dimension(larguraBgPrincipal,alturaBgPrincipal));
		bgPrincipal.setMaximumSize(new Dimension(larguraBgPrincipal*10,alturaBgPrincipal*10));
		bgPrincipal.setMinimumSize(new Dimension(larguraBgPrincipal/15,alturaBgPrincipal/15));

		
		produtoBg = new JPanel(new GridBagLayout());
		produtoBg.setPreferredSize(new Dimension(larguraBgPrincipal/2-larguraBgQuantidadeMax/2,
				alturaBgPrincipal));
		
		quantidadeBg = new JPanel(new GridBagLayout());
		quantidadeBg.setPreferredSize(new Dimension(larguraBgPrincipal/2-larguraBgQuantidadeMax/2,
				alturaBgPrincipal));
		
		infoBg = new JPanel();
		infoBg.setLayout(new BoxLayout(infoBg, BoxLayout.PAGE_AXIS));
		infoBg.setBorder(new EmptyBorder(40,0,0,0));
		infoBg.setPreferredSize(new Dimension(larguraBgQuantidadeMax, alturaBgPrincipal+20));
		
		observacoesBg = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
		observacoesBg.setPreferredSize(new Dimension(600, 30));
		observacoesBg.setMinimumSize(new Dimension(600, 30));
		
		//Configurar Labels 1/2
		produtoLabel = new JLabel("Produto: ");
		quantidadeLabel = new JLabel("Quantidade: ");
		observacoesLabel = new JLabel("Observações: ");
		
		quantidadeMaxLabel = new JLabel();
		precoInfoLabel = new JLabel();
		quantidadeMinLabel = new JLabel();
		
		//Configurar ComboBox
		quantidadeBox = new JComboBox<Integer>();
		
		quantidadeBox.removeAllItems();
		produtoBox.removeAllItems();
		
		atualizarProdutoComboBox(produtosCadastrados);
		
		int quantidadesDoPrimeiro = produtosCadastrados.getQuantidades().get( produtosCadastrados.getProdutos().indexOf( 
				produtoBox.getSelectedItem() ) );
		
		//preencher quantidades ComboBox
		for(int i = 1; i <= quantidadesDoPrimeiro; i++){
			quantidadeBox.addItem(i);
		}
		//
		
		produtoListener = new SelecionarProdutoC().new ComboBoxListener(this);
		
		produtoBox.addItemListener(produtoListener);
		
		//Configurar JLabel 2/2
		quantidadeMaxLabel.setText("Em estoque: " + quantidadesDoPrimeiro);
		precoInfoLabel.setText("Preço: " + produtosCadastrados.getPrecos().get(0));
		quantidadeMinLabel.setText("Mínimo: " + produtosCadastrados.getQuantidadesMin().get(0));
		
		//configurar campos de texto
		observacoes = new JFormattedTextField();
		observacoes.setColumns(30);
		
		//Configurar Limites
		produtoLabelLim = new GridBagConstraints();
		produtoLabelLim.gridx = 0;
		produtoLabelLim.gridy = 0;
		
		observacoesLim = new GridBagConstraints();
		observacoesLim.gridx = 0;
		observacoesLim.gridy = 2;
		
		produtoBoxLim = new GridBagConstraints();
		produtoBoxLim.gridx = 0;
		produtoBoxLim.gridy = 1;
		
		quantidadeLabelLim = new GridBagConstraints();
		quantidadeLabelLim.gridx = 0;
		quantidadeLabelLim.gridy = 0;
		
		quantidadeBoxLim = new GridBagConstraints();
		quantidadeBoxLim.gridx = 0;
		quantidadeBoxLim.gridy = 1;
	}

	public void inserir(MainFrameV mainFrame) {
		mainFrame.getMainFrame().add(bgPrincipal);
		adicionarNoBg();
	}

	public JPanel get() {
		adicionarNoBg();
		return bgPrincipal;
	}
	
	private void adicionarNoBg(){
		bgPrincipal.add(produtoBg);
		bgPrincipal.add(quantidadeBg);
		bgPrincipal.add(infoBg);
		bgPrincipal.add(observacoesBg, observacoesLim);
		
		produtoBg.add(produtoLabel);
		produtoBg.add(produtoBox, produtoBoxLim);
		produtoBg.add(produtoLabel, produtoLabelLim);
		
		observacoesBg.add(observacoesLabel);
		observacoesBg.add(observacoes);
		
		quantidadeBg.add(quantidadeLabel);
		quantidadeBg.add(quantidadeBox, quantidadeBoxLim);
		quantidadeBg.add(quantidadeLabel, quantidadeLabelLim);
		
		infoBg.add(quantidadeMaxLabel);
		infoBg.add(quantidadeMinLabel);
		infoBg.add(precoInfoLabel);
	}
	
	public void atualizarProdutoComboBox(ProdutoM produtosCadastrados){
		
		this.produtosCadastrados = produtosCadastrados;
		
		produtoBox.removeAllItems();
		produtosCadastrados.getProdutos().clear();
		produtosCadastrados.getPrecos().clear();
		produtosCadastrados.getQuantidades().clear();
		produtosCadastrados.getQuantidadesMin().clear();
		
		produtosCadastrados.leiaArquivo();
		
		for(int i = 0; i < produtosCadastrados.getProdutos().size(); i++){
			if(produtosCadastrados.getQuantidades().get(i) > produtosCadastrados.getQuantidadesMin().get(i)){
				
				produtoBox.addItem(produtosCadastrados.getProdutos().get(i));
			}
		}
		if(produtoBox.getItemCount() == 0){
			produtoBox.addItem(produtosCadastrados.getProdutos().get(0));
			semEstoque = true;
		}
	}
	public void atualizarProdutoComboBoxOcultos(ProdutoM produtosCadastrados){
		produtoBox.removeAllItems();
		produtosCadastrados.getProdutos().clear();
		produtosCadastrados.getPrecos().clear();
		produtosCadastrados.getQuantidades().clear();
		produtosCadastrados.getQuantidadesMin().clear();
		
		produtosCadastrados.leiaArquivo();
		
		for(int i = 0; i < produtosCadastrados.getProdutos().size(); i++){
				
			produtoBox.addItem(produtosCadastrados.getProdutos().get(i));
		}
	}
	
	public void atualizarProdutoComboBoxOcultos(ProdutoM produtosCadastrados, JComboBox<String> produtoBox){
		this.produtoBox = produtoBox;
		this.produtosCadastrados = produtosCadastrados;
		
		produtoBox.removeAllItems();
		produtosCadastrados.getProdutos().clear();
		produtosCadastrados.getPrecos().clear();
		produtosCadastrados.getQuantidades().clear();
		produtosCadastrados.getQuantidadesMin().clear();
		
		produtosCadastrados.leiaArquivo();
		
		for(int i = 0; i < produtosCadastrados.getProdutos().size(); i++){
				
			produtoBox.addItem(produtosCadastrados.getProdutos().get(i));
		}
	}
	
	public void atualizarProdutoComboBox(ProdutoM produtosCadastrados, JComboBox<String> produtoBox){
		this.produtoBox = produtoBox;
		this.produtosCadastrados = produtosCadastrados;
		
		produtoBox.removeAllItems();
		produtosCadastrados.getProdutos().clear();
		produtosCadastrados.getPrecos().clear();
		produtosCadastrados.getQuantidades().clear();
		produtosCadastrados.getQuantidadesMin().clear();
		
		produtosCadastrados.leiaArquivo();
		
		for(int i = 0; i < produtosCadastrados.getProdutos().size(); i++){
			if(produtosCadastrados.getQuantidades().get(i) > produtosCadastrados.getQuantidadesMin().get(i)){
				
				produtoBox.addItem(produtosCadastrados.getProdutos().get(i));
			}
		}
		if(produtoBox.getItemCount() == 0){
			produtoBox.addItem(produtosCadastrados.getProdutos().get(0));
			semEstoque = true;
		}
	}
	
	public JLabel getQuantidadeMinLabel() {
		return quantidadeMinLabel;
	}

	public void setQuantidadeMinLabel(JLabel quantidadeMinLabel) {
		this.quantidadeMinLabel = quantidadeMinLabel;
	}

	public JFormattedTextField getObservacoes() {
		return observacoes;
	}

	public JLabel getPrecoInfoLabel() {
		return precoInfoLabel;
	}

	public void setPrecoInfoLabel(JLabel precoInfoLabel) {
		this.precoInfoLabel = precoInfoLabel;
	}

	public boolean isSemEstoque() {
		return semEstoque;
	}
	public JPanel getQuantidadeBg() {
		return quantidadeBg;
	}
	public JLabel getQuantidadeMaxLabel() {
		return quantidadeMaxLabel;
	}
	public ProdutoM getProdutosCadastrados() {
		return produtosCadastrados;
	}
	public int getLarguraBgPrincipal() {
		return larguraBgPrincipal;
	}
	public int getAlturaBgPrincipal() {
		return alturaBgPrincipal;
	}
	public JComboBox<String> getProdutoBox() {
		return produtoBox;
	}
	public JComboBox<Integer> getQuantidadeBox() {
		return quantidadeBox;
	}
	public JPanel getInfoBg() {
		return infoBg;
	}
}

package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MenuPrincipalC;
import view.LoginAndCadastrosV.ContainersInternosV;

public class MenuPrincipalV implements ContainersInternosV{
	//Panels
	private JPanel bgPrincipal; 
	private JPanel boxBg;
	private JPanel buttonsBg;
	private JPanel headerBg;
	
	//Botões
	private JButton cadastrarPedido;
	private JButton cadastrarProduto;
	private JButton cadastrarFuncionario;
	private JButton trocarUser;
	private JButton relatorioVendas;
	private JButton modificarProduto;
	
	//Labels
	private JLabel header;
	
	//Listener
	private MenuPrincipalC.ButtonClickListener listener; 

	public MenuPrincipalV(){
		//Configurar Containers
		bgPrincipal = new JPanel(new BorderLayout(0, 0));
		
		boxBg = new JPanel();
		boxBg.setLayout(new BorderLayout());
		boxBg.setPreferredSize(new Dimension(800, 800));

		buttonsBg = new JPanel();
		buttonsBg.setLayout(new BoxLayout(buttonsBg, BoxLayout.PAGE_AXIS));
		buttonsBg.setMinimumSize(new Dimension(192, 300));
		buttonsBg.setBorder(new EmptyBorder(0, 0, 40, 0));
		
		headerBg = new JPanel(new FlowLayout(FlowLayout.CENTER, 0 , 0));
		headerBg.setPreferredSize(new Dimension(200, 100));
		headerBg.setBorder(new EmptyBorder(40, 0, 80, 0));
		
		//Configuar botões
		listener = new MenuPrincipalC().new ButtonClickListener(bgPrincipal);
		
		cadastrarPedido = new JButton("Cadastrar Pedido        ");
		cadastrarPedido.setActionCommand("cadastrar pedido");
		cadastrarPedido.addActionListener(listener);
		
		cadastrarProduto = new JButton("Cadastrar Produto      ");
		cadastrarProduto.setActionCommand("cadastrar produto");
		cadastrarProduto.addActionListener(listener);
		
		cadastrarFuncionario = new JButton("Cadastrar Funcionário");
		cadastrarFuncionario.setActionCommand("cadastrar funcionario");
		cadastrarFuncionario.addActionListener(listener);
		
		trocarUser = new JButton("Trocar de usuário        ");
		trocarUser.setActionCommand("trocar user");
		trocarUser.addActionListener(listener);
		
		relatorioVendas = new JButton("Relatório de vendas    ");
		relatorioVendas.setActionCommand("relatorio de vendas");
		relatorioVendas.addActionListener(listener);
		
		modificarProduto = new JButton("Modificar Produto        ");
		modificarProduto.setActionCommand("modificar produto");
		modificarProduto.addActionListener(listener);
		
		//Configurar Labels
		header = new JLabel("ESCOLHA UMA AÇÃO");
	}
	
	public MenuPrincipalC.ButtonClickListener getListener() {
		return listener;
	}
	
	public void inserir(MainFrameV mainFrame) {
		mainFrame.getMainFrame().add(bgPrincipal);
		adicionarNoBg();
	}
	
	private void adicionarNoBg(){
		int espacoEntreBotoes = 20;
		
		bgPrincipal.add(boxBg, BorderLayout.CENTER);
		
		boxBg.add(headerBg, BorderLayout.NORTH);
		boxBg.add(buttonsBg, BorderLayout.SOUTH);
		
		headerBg.add(header);
		
		buttonsBg.add(cadastrarPedido);
		buttonsBg.add(Box.createRigidArea(new Dimension(0, espacoEntreBotoes)));
		buttonsBg.add(cadastrarProduto);
		buttonsBg.add(Box.createRigidArea(new Dimension(0, espacoEntreBotoes)));
		buttonsBg.add(cadastrarFuncionario);
		buttonsBg.add(Box.createRigidArea(new Dimension(0, espacoEntreBotoes)));
		buttonsBg.add(modificarProduto);
		buttonsBg.add(Box.createRigidArea(new Dimension(0, espacoEntreBotoes)));
		buttonsBg.add(relatorioVendas);
		buttonsBg.add(Box.createRigidArea(new Dimension(0, espacoEntreBotoes)));
		buttonsBg.add(trocarUser);
	}

	public JPanel get() {
		adicionarNoBg();
		return bgPrincipal;
	}
}

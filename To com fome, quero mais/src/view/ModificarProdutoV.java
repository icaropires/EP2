///Bg = BackGround

package view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.MaskFormatter;

import controller.ModificarProdutoC;
import model.ProdutoM;
import view.LoginAndCadastrosV.ContainersInternosV;;

public class ModificarProdutoV extends LoginAndCadastrosV implements ContainersInternosV{
	//Containers
	private JPanel precoBg;
	private JPanel quantidadeBg;
	private JPanel quantidadeMinBg;
	private JPanel produtoBg;
	
	//Constraints
	private GridBagConstraints limitesExternos; //no mainFrame
	private GridBagConstraints limitesHeader;
	private GridBagConstraints limitesNome;
	private GridBagConstraints limitesProduto;
	private GridBagConstraints limitesPreco;
	private GridBagConstraints limitesQuantidade;
	private GridBagConstraints limitesQuantidadeMin;
	private GridBagConstraints limitesBotoes;
	
	//Labels
	private JLabel produtoLabel;
	private JLabel precoLabel;
	private JLabel quantidadeLabel;
	private JLabel quantidadeMinLabel;
	
	//Campos de texto
	private JFormattedTextField preco;
	private JFormattedTextField quantidade;
	private JFormattedTextField quantidadeMin;

	//Máscaras
	private MaskFormatter precoMask;
	private MaskFormatter quantidadeMask;
	
	//ComboBox
	private JComboBox<String> produtoBox;
	
	//Botões
	private JButton excluirProduto;
	
	//Listener
	private ModificarProdutoC.ButtonClickListener listener;
	
	public ModificarProdutoV(){
		//ConainersCadastroV(nomeString, headerString, tamanhoMask, confirmarString)
		super("Novo nome", "INSIRA AS NOVAS INFORMAÇÕES", 
		"******************************", "Modificar Produto");
		//Configurar máscaras
		try{
			precoMask = new MaskFormatter("R$###,##");
			precoMask.setValueContainsLiteralCharacters(false);  
			precoMask.setPlaceholderCharacter('_');
			
		}catch(ParseException e){
			e.printStackTrace();
		}
		
		try{
			quantidadeMask = new MaskFormatter("###");
			quantidadeMask.setPlaceholderCharacter('_');
			
		}catch(ParseException e){
			e.printStackTrace();
		}
		
		//Configurar Containers
		bgPrincipal.setPreferredSize(new Dimension(500, 250));
		
		produtoBg = new JPanel(new FlowLayout());
		precoBg = new JPanel(new FlowLayout());
		quantidadeBg = new JPanel(new FlowLayout());
		quantidadeMinBg = new JPanel(new FlowLayout());
		botoesBg = new JPanel(new FlowLayout());
		
		//Configurar posição no MainFrame
		limitesExternos = new GridBagConstraints();
		limitesExternos.gridx = 0;
		limitesExternos.gridy = 0;
		limitesExternos.weightx = 1.0;
		limitesExternos.weighty = 1.0;
		limitesExternos.anchor = GridBagConstraints.CENTER;
		
		limitesHeader = new GridBagConstraints();
		limitesHeader.gridx = 0;
		limitesHeader.gridy = 0;
		limitesHeader.weightx = 1.0;
		limitesHeader.fill = GridBagConstraints.BOTH;
		limitesHeader.anchor = GridBagConstraints.NORTH;
		limitesHeader.insets = new Insets(0, 0, 10, 0);
		
		limitesProduto = new GridBagConstraints();
		limitesProduto.gridx = 0;
		limitesProduto.gridy = 4;
		limitesProduto.gridwidth = 5;
		limitesProduto.weightx = 1.0;
		limitesProduto.anchor = GridBagConstraints.WEST;
		
		limitesNome = new GridBagConstraints();
		limitesNome.gridx = 0;
		limitesNome.gridy = 5;
		limitesNome.gridwidth = 6;
		limitesNome.weightx = 1.0;
		limitesNome.anchor = GridBagConstraints.WEST;
		
		limitesPreco = new GridBagConstraints();
		limitesPreco.gridx = 0;
		limitesPreco.gridy = limitesNome.gridy + 1;
		limitesPreco.gridwidth = 5;
		limitesPreco.weightx = 1.0;
		limitesPreco.anchor = GridBagConstraints.WEST;
		
		limitesQuantidade = new GridBagConstraints();
		limitesQuantidade.gridx = 0;
		limitesQuantidade.gridy = limitesNome.gridy + 2;
		limitesQuantidade.gridwidth = 5;
		limitesQuantidade.weightx = 1.0;
		limitesQuantidade.anchor = GridBagConstraints.WEST;
		
		limitesQuantidadeMin = new GridBagConstraints();
		limitesQuantidadeMin.gridx = 0;
		limitesQuantidadeMin.gridy = limitesNome.gridy + 3;
		limitesQuantidadeMin.gridwidth = 5;
		limitesQuantidadeMin.weightx = 1.0;
		limitesQuantidadeMin.anchor = GridBagConstraints.WEST;
		
		limitesBotoes = new GridBagConstraints();
		limitesBotoes.gridx = 0;
		limitesBotoes.gridy = limitesNome.gridy + 4;
		limitesBotoes.gridwidth = 5;
		limitesBotoes.weightx = 1.0;
		limitesBotoes.anchor = GridBagConstraints.SOUTH;
		
		//Configurar Labels
		produtoLabel = new JLabel("Produto que será alterado");
		precoLabel = new JLabel("Preço: ", JLabel.LEFT);
		quantidadeLabel = new JLabel("Quantidade: ", JLabel.LEFT);
		quantidadeMinLabel = new JLabel("Quantidade Mínima: ", JLabel.LEFT);
		
		//Configurar Campos de texto		
		nome.setColumns(19);;
		
		preco = new JFormattedTextField(precoMask);
		preco.setColumns(6);
		
		quantidade = new JFormattedTextField(quantidadeMask);
		quantidade.setColumns(3);
		
		quantidadeMin = new JFormattedTextField(quantidadeMask);
		quantidadeMin.setColumns(3);
		
		//Configurar ComboBox
		produtoBox = new JComboBox<String>();
		new SelecionarProdutoV().atualizarProdutoComboBoxOcultos(new ProdutoM(), produtoBox);
		
		//Configurar Botões
		listener = new ModificarProdutoC().new ButtonClickListener(nome, preco, quantidade,	quantidadeMin, produtoBox,this.bgPrincipal);
		
		excluirProduto = new JButton("Exluir Produto");
		excluirProduto.setActionCommand("excluir produto");
		excluirProduto.addActionListener(listener);
		
		setComumActions(listener);
		
	}
	
	public void inserir(MainFrameV mainFrame){
		mainFrame.getMainFrame().add(bgPrincipal, limitesExternos);
		
		adicionarNoBg();
	}
	
	public JPanel get(){
		adicionarNoBg();
		return bgPrincipal;
	}
	
	private void adicionarNoBg(){
		
		bgPrincipal.add(headerBg, limitesHeader);
		bgPrincipal.add(nomeBg, limitesNome);
		bgPrincipal.add(produtoBg, limitesProduto);
		bgPrincipal.add(precoBg, limitesPreco);
		bgPrincipal.add(quantidadeBg, limitesQuantidade);
		bgPrincipal.add(quantidadeMinBg, limitesQuantidadeMin);
		bgPrincipal.add(botoesBg, limitesBotoes);
		
		headerBg.add(headerLabel);
		
		nomeBg.add(nomeLabel);
		nomeBg.add(nome);
		
		produtoBg.add(produtoLabel);
		produtoBg.add(produtoBox);
		
		precoBg.add(precoLabel);
		precoBg.add(preco);
		
		quantidadeBg.add(quantidadeLabel);
		quantidadeBg.add(quantidade);
		
		quantidadeMinBg.add(quantidadeMinLabel);
		quantidadeMinBg.add(quantidadeMin);
		
		botoesBg.add(confirmar);
		botoesBg.add(cancelar);
		botoesBg.add(excluirProduto);
	}
	public ModificarProdutoC.ButtonClickListener getListener() {
		return listener;
	}
}
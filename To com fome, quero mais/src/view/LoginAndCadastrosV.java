package view;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.MaskFormatter;

abstract public class LoginAndCadastrosV {
	//Containers
	protected JPanel bgPrincipal;
	protected JPanel headerBg;
	protected JPanel nomeBg;
	protected JPanel botoesBg;
	
	//Campos de texto
	protected JFormattedTextField nome;
	
	//Labels
	protected JLabel nomeLabel;
	protected JLabel headerLabel;
	
	//Mascaras
	MaskFormatter nomeMask;
	
	//Botões
	protected JButton confirmar;
	protected JButton cancelar;
	
	//Strings
	String nomeString;
	String headerString;
	String tamanhoMask;
	String confirmarString;

	//(nomeString, headerString, tamanhoMask, confirmarString)
	public LoginAndCadastrosV(String nomeString, String headerString, String tamanhoMask, 
			String confirmarString) {
		
		this.nomeString = nomeString;
		this.headerString = headerString;
		this.tamanhoMask = tamanhoMask;
		this.confirmarString = confirmarString;
		
		//Configurar Conteiner
		bgPrincipal = new JPanel(new GridBagLayout());
		headerBg = new JPanel(new FlowLayout());
		nomeBg = new JPanel();
		botoesBg = new JPanel(new FlowLayout());
		
		//Configurar Mascara
		//tamanhoMask sempre será **** variando somente o tamanho
		try{
			nomeMask = new MaskFormatter(tamanhoMask);
			nomeMask.setValidCharacters("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ.áéíóúÁÉÍÓÚã/ç");
			nomeMask.setPlaceholderCharacter(' ');
		}
		catch(ParseException e){
			e.printStackTrace();
		}
		
		//Configurar Labels
		nomeLabel = new JLabel(nomeString, JLabel.LEFT);
		headerLabel = new JLabel(headerString, JLabel.CENTER);
		
		nome = new JFormattedTextField(nomeMask);
		
		//Configurar botões
		confirmar = new JButton(confirmarString);
		confirmar.setActionCommand("confirmar");
		
		cancelar = new JButton("Cancelar");
		cancelar.setActionCommand("cancelar");
	}
	
	public JPanel getBgPrincipal() {
		return bgPrincipal;
	}
	
	public void setComumActions(ActionListener listener){
		confirmar.setActionCommand("confirmar");
		confirmar.addActionListener(listener);
		
		cancelar.setActionCommand("cancelar");
		cancelar.addActionListener(listener);
	}
	
	public interface ContainersInternosV{
		public void inserir(MainFrameV mainFrame);
		public JPanel get();
	}
}

///Bg = BackGround
package view;

import java.awt.*;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import controller.CadastrarFuncionarioC;
import view.LoginAndCadastrosV.ContainersInternosV;

public class CadastrarFuncionarioV extends LoginAndCadastrosV implements ContainersInternosV{
	//Containers
	private JPanel senhaBg;
	private JPanel senhaAdmBg;
	
	//Constraints
	private GridBagConstraints limitesExternos; //no mainFrame
	private GridBagConstraints limitesHeader;
	private GridBagConstraints limitesNome;
	private GridBagConstraints limitesSenha;
	private GridBagConstraints limitesSenhaAdm;	
	private GridBagConstraints limitesBotoes;
	
	//Labels
	private JLabel senhaLabel;
	private JLabel senhaAdmLabel;
	
	//Campos de texto
	private JPasswordField senha;
	private JPasswordField senhaAdm;
	
	//Listener
	private CadastrarFuncionarioC.ButtonClickListener listener;
	
	public CadastrarFuncionarioV(){
		//ConainersCadastroV(nomeString, headerString, tamanhoMask, confirmarString)
		super("Nome do funcionário", "INSIRA AS INFORMAÇÕES DO FUNCIONÁRIO", 
		"******************************", "Cadastrar funcionário");
		
		//Configurar Containers
		bgPrincipal.setPreferredSize(new Dimension(500, 200));
		bgPrincipal.setMaximumSize(new Dimension(900, 600));
		bgPrincipal.setMinimumSize(new Dimension(100, 150));
		
		senhaBg = new JPanel(new FlowLayout());
		senhaAdmBg = new JPanel(new FlowLayout());
		
		//Configurar posição no MainFrame
		limitesExternos = new GridBagConstraints();
		limitesExternos.gridx = 0;
		limitesExternos.gridy = 0;
		limitesExternos.weightx = 1.0;
		limitesExternos.weighty = 1.0;
		limitesExternos.anchor = GridBagConstraints.CENTER;
		
		limitesHeader = new GridBagConstraints();
		limitesHeader.gridx = 0;
		limitesHeader.gridy = 0;
		limitesHeader.weightx = 1.0;
		limitesHeader.fill = GridBagConstraints.BOTH;
		limitesHeader.anchor = GridBagConstraints.NORTH;
		limitesHeader.insets = new Insets(0, 0, 10, 0);
		
		limitesNome = new GridBagConstraints();
		limitesNome.gridx = 0;
		limitesNome.gridy = 4;
		limitesNome.gridwidth = 5;
		limitesNome.weightx = 1.0;
		limitesNome.anchor = GridBagConstraints.WEST;
		
		limitesSenha = new GridBagConstraints();
		limitesSenha.gridx = 0;
		limitesSenha.gridy = limitesNome.gridy + 1;
		limitesSenha.gridwidth = 5;
		limitesSenha.weightx = 1.0;
		limitesSenha.anchor = GridBagConstraints.WEST;
		
		limitesSenhaAdm = new GridBagConstraints();
		limitesSenhaAdm.gridx = 0;
		limitesSenhaAdm.gridy = limitesNome.gridy + 2;
		limitesSenhaAdm.gridwidth = 5;
		limitesSenhaAdm.weightx = 1.0;
		limitesSenhaAdm.anchor = GridBagConstraints.WEST;
		
		limitesBotoes = new GridBagConstraints();
		limitesBotoes.gridx = 0;
		limitesBotoes.gridy = limitesNome.gridy + 3;
		limitesBotoes.gridwidth = 5;
		limitesBotoes.weightx = 1.0;
		limitesBotoes.anchor = GridBagConstraints.SOUTH;
		
		//Configurar Labels
		senhaLabel = new JLabel("Senha do funcionário: ", JLabel.LEFT);
		senhaAdmLabel = new JLabel("Senha do administrador: ", JLabel.LEFT);
		
		//Configurar Campos de texto
		nome.setColumns(21);
		
		senha = new JPasswordField();
		senha.setColumns(8);
		
		
		senhaAdm = new JPasswordField();
		senhaAdm.setColumns(8);
		
		//Configurar botões
		listener = new CadastrarFuncionarioC().new ButtonClickListener(nome,  senha, senhaAdm, this.bgPrincipal);
		
		setComumActions(listener);
	}
	
	public void inserir(MainFrameV mainFrame){
		
		mainFrame.getMainFrame().add(bgPrincipal, limitesExternos);
		adicionarNoBg();
	}

	public JPanel get() {
	
		adicionarNoBg();
		return bgPrincipal;
	}
	
	private void adicionarNoBg(){
		bgPrincipal.add(headerBg, limitesHeader);
		bgPrincipal.add(nomeBg, limitesNome);
		bgPrincipal.add(senhaBg, limitesSenha);
		bgPrincipal.add(senhaAdmBg, limitesSenhaAdm);
		bgPrincipal.add(botoesBg, limitesBotoes);
		
		headerBg.add(headerLabel);
		
		nomeBg.add(nomeLabel);
		nomeBg.add(nome);
		
		senhaBg.add(senhaLabel);
		senhaBg.add(senha);
		
		senhaAdmBg.add(senhaAdmLabel);
		senhaAdmBg.add(senhaAdm);
		
		botoesBg.add(confirmar);
		botoesBg.add(cancelar);
	}
	public CadastrarFuncionarioC.ButtonClickListener getListener() {
		return listener;
	}


}

package view;

import java.awt.GridBagLayout;
import javax.swing.JFrame;

public class MainFrameV {
	private JFrame mainFrame;
	
	public MainFrameV(){
		mainFrame = new JFrame("To com Fome, quero mais - Login");
		mainFrame.setSize(1000, 700);
		mainFrame.setLocation(200,100);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setLayout(new GridBagLayout());
	}

	public JFrame getMainFrame() {
		return mainFrame;
	}

	public void setMainFrame(JFrame mainFrame) {
		this.mainFrame = mainFrame;
	}
}

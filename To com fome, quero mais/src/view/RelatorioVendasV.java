package view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import controller.RelatorioVendasC;
import model.ProdutoM;
import view.LoginAndCadastrosV.ContainersInternosV;

public class RelatorioVendasV implements ContainersInternosV{
	//Containers
	private JPanel bgPrincipal;
	private JPanel headerBg;
	private JPanel buttonBg;
	private JTextArea textoBg;
	private JScrollPane scrollBg;
	
	//Labels
	private JLabel headerLabel;
	
	//Botões
	private JButton ok; 
	
	//listener
	private RelatorioVendasC.ButtonClickListener listener;
	
	public RelatorioVendasV(){
		//Configurar Containers
		bgPrincipal = new JPanel();
		bgPrincipal.setLayout(new BoxLayout(bgPrincipal, BoxLayout.PAGE_AXIS));
		bgPrincipal.setPreferredSize(new Dimension(600, 600));
		
		headerBg = new JPanel(new FlowLayout());
		headerBg.setMaximumSize(new Dimension(600, 100));
		
		buttonBg = new JPanel(new FlowLayout());
		buttonBg.setMaximumSize(new Dimension(600, 15));
		
		textoBg = new JTextArea();
		textoBg.setEditable(false);
		
		scrollBg = new JScrollPane(textoBg);
		
		//Configurar Label
		headerLabel = new JLabel("RELATÓRIO DE VENDAS");
		
		//ConfigurarListener
		listener = new RelatorioVendasC().new ButtonClickListener(bgPrincipal);
		
		//configurar botões
		ok = new JButton("OK");
		ok.setActionCommand("ok");
		ok.addActionListener(listener);
		

		atualizarTexto();
	}
	
	public void atualizarTexto(){
		try{
			
			ProdutoM.criarArquivo("#VENDAS REALIZADAS", "vendas");
			Scanner scanner = new Scanner(new FileReader("./defaults/vendas"));
			textoBg.setText("");
			
			/*Pular cabeçalho*/
			scanner.nextLine();
			scanner.nextLine();
			/*				*/
				
			while(scanner.hasNext()){
				textoBg.append("No dia " + scanner.nextLine());
				textoBg.append(" às " + scanner.nextLine() + " o vendedor(a) ");
				textoBg.append(scanner.nextLine().trim() + " vendeu:\n");
				scanner.nextLine();
				
				String teste = "#";
				while(!teste.equals("@") ){
					
					if(!teste.equals("@")) teste = scanner.nextLine();
					if(!teste.equals("@")) textoBg.append(teste.trim() + " x ");

					if(!teste.equals("@")) teste = scanner.nextLine();
					if(!teste.equals("@")) textoBg.append(teste + "\n" + "Observação sobre o pedido: ");
					
					if(!teste.equals("@")) teste = scanner.nextLine();
					if(!teste.equals("@")) textoBg.append(teste.trim() + "\n");
					
				}
				
				textoBg.append("para o(a) Sr(a) " + scanner.nextLine().trim() + " que estava na mesa " + scanner.nextLine());
				textoBg.append(" e pagou com " + scanner.nextLine());
				textoBg.append("\n--------------------------------------------------------------------------------------------------------------------------------------------\n");
			}
			scanner.close();
		}
		
		catch(IOException e){
			System.out.println("Não foi possível criar o arquivo de vendas");
		}
	}
	
	public void adicionarNoBg(){
		bgPrincipal.add(headerBg);
		bgPrincipal.add(scrollBg);
		bgPrincipal.add(buttonBg);
		bgPrincipal.add(ok);
		headerBg.add(headerLabel);
		buttonBg.add(ok);
	}
	
	public void inserir(MainFrameV mainFrame) {
		mainFrame.getMainFrame().add(bgPrincipal);
		adicionarNoBg();
	}

	public JPanel get() {
		adicionarNoBg();
		return bgPrincipal;
	}
	public RelatorioVendasC.ButtonClickListener getListener() {
		return listener;
	} 
	
}

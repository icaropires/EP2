//Bg = Background

package view;

import view.MainFrameV;
import java.awt.*;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import controller.LoginC;
import view.LoginAndCadastrosV.ContainersInternosV;

public class LoginV extends LoginAndCadastrosV implements ContainersInternosV{
	//Containers
	private JPanel loginGridBg;
	
	private JPanel identificacaoBg;
	
	private JPanel senhaBg;
	private JPanel senhaTextBg;
	
	private JPanel buttonsAndStatusBg;
	
	//Labels
	private JLabel senhaLabel;
	
	//Text Fields
	private JPasswordField senha;
	
	//Botões
	private JButton cadastrarUsuarioButton;
	@SuppressWarnings("unused")
	private MainFrameV mainFrame;
	
	//Listener
	LoginC.ButtonClickListener listener;
	
	public LoginV(MainFrameV mainFrame){
		
		//ConainersCadastroV(nomeString, headerString, tamanhoMask, confirmarString)
		super("Nome (usado no cadastro)", "INSIRA SEUS DADOS","******************************", 
			  "Entrar");
		
		this.mainFrame = mainFrame;
		
		//Configurar Containers
		bgPrincipal = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
		
		loginGridBg = new JPanel(new GridLayout(4,1));
		loginGridBg.setMinimumSize(new Dimension(380,250));
		loginGridBg.setMaximumSize(new Dimension(380,250));
		
		identificacaoBg = new JPanel(new GridLayout(2,1)); 		//fundo Label + caixa de texto
		nomeBg = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0)); //fundo caixa de texto
		
		senhaBg = new JPanel(new GridLayout(2,1)); 						//fundo do Label + caixa de texto
		senhaTextBg = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0)); //fundo caixa de texto
		
		buttonsAndStatusBg = new JPanel(new GridLayout(2, 1));
		
		//Configurar Text Fields
		nome.setColumns(27);
		senha = new JPasswordField(5);
		
		//Configurar Labels
		senhaLabel = new JLabel("Senha:", JLabel.LEFT);
		
		//Configurar Botões
		listener = new LoginC().new ButtonClickListener(nome, senha, this.bgPrincipal, mainFrame);
		
		setComumActions(listener);
		
		cadastrarUsuarioButton = new JButton("Cadastrar um funcionário");
		cadastrarUsuarioButton.setActionCommand("cadastrar funcionario");
		cadastrarUsuarioButton.addActionListener(listener);
	}
	
	public void inserir(MainFrameV mainFrame) {
		mainFrame.getMainFrame().add(bgPrincipal);
		adicionarNoBg();
	}

	public JPanel get() {
		adicionarNoBg();
		return bgPrincipal;
	}
	
	private void adicionarNoBg(){
		bgPrincipal.add(loginGridBg);
		
		loginGridBg.add(headerLabel);
		
		loginGridBg.add(identificacaoBg);
		identificacaoBg.add(nomeLabel);
		identificacaoBg.add(nomeBg);
		nomeBg.add(nome);
		
		loginGridBg.add(senhaBg);
		senhaBg.add(senhaLabel);
		senhaBg.add(senhaTextBg);
		senhaTextBg.add(senha);
		
		loginGridBg.add(buttonsAndStatusBg);
		buttonsAndStatusBg.add(botoesBg);
		botoesBg.add(confirmar);
		botoesBg.add(cadastrarUsuarioButton);
	}
	public LoginC.ButtonClickListener getListener() {
		return listener;
	}
}
package view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.text.ParseException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.text.MaskFormatter;

import controller.CadastrarPedidoC;
import view.LoginAndCadastrosV.ContainersInternosV;

public class CadastrarPedidoV extends LoginAndCadastrosV implements ContainersInternosV{
	//Containers
	private JPanel mesaBg;
	private JPanel produtosBg;
	private JPanel formaDePagamentoBg;
	
	//Constraints
	private GridBagConstraints limitesExternos; //no mainFrame
	private GridBagConstraints limitesHeader;
	private GridBagConstraints limitesNome;
	private GridBagConstraints limitesMesa;
	private GridBagConstraints limitesFormaDePagamento;	
	private GridBagConstraints limitesBotoes;
	private GridBagConstraints limitesProdutos;
	
	//Panel com rolagem
	private JScrollPane produtosBgScroll;
	
	//Labels
	private JLabel mesaLabel;
	private JLabel formaDePagamentoLabel;
	
	//Combo Box
	private JComboBox<String> formaDePagamento;
	
	//Campos de texto
	private JFormattedTextField mesa;
	
	//Mascaras
	private MaskFormatter mesaMask;
	
	//Botões
	private JButton adicionarProduto;
	private JButton removerProduto;

	//
	private SelecionarProdutoV menuProduto;
	
	//Listener
	private CadastrarPedidoC.ButtonClickListener listener;
	
	public CadastrarPedidoV(String vendedor) {
		super("Nome do Cliente: ", "REALIZAR PEDIDO", 
				"******************************", "Terminar");
		
		//
		menuProduto = new SelecionarProdutoV();
		
		//Configurar Containers
		bgPrincipal.setPreferredSize(new Dimension(new SelecionarProdutoV().getLarguraBgPrincipal()+7, 600));
		bgPrincipal.setMaximumSize(new Dimension(900, 900));
		bgPrincipal.setMinimumSize(new Dimension(100, 600));
		
		mesaBg = new JPanel(new FlowLayout());
		formaDePagamentoBg = new JPanel(new FlowLayout());
		
		produtosBg = new JPanel();
		produtosBg.setLayout(new BoxLayout(produtosBg, BoxLayout.PAGE_AXIS));
		produtosBg.setPreferredSize(new Dimension(new Dimension(new SelecionarProdutoV().getLarguraBgPrincipal(), 100)));
		
		produtosBgScroll = new JScrollPane(produtosBg);
		produtosBgScroll.setPreferredSize(new Dimension(new SelecionarProdutoV().getLarguraBgPrincipal()+7,300));
		//produtosBgScroll.
		
		//Configurar Mask
		try{
			mesaMask = new MaskFormatter("##");
			mesaMask.setPlaceholderCharacter('0');
			mesaMask.setValueContainsLiteralCharacters(false);
		}
		catch(ParseException e){
			e.printStackTrace();
		}
		
		//Configurar posição no MainFrame
		limitesExternos = new GridBagConstraints();
		limitesExternos.gridx = 0;
		limitesExternos.gridy = 0;
		limitesExternos.weightx = 1.0;
		limitesExternos.weighty = 1.0;
		limitesExternos.anchor = GridBagConstraints.CENTER;
		
		limitesHeader = new GridBagConstraints();
		limitesHeader.gridx = 0;
		limitesHeader.gridy = 0;
		limitesHeader.weightx = 1.0;
		limitesHeader.fill = GridBagConstraints.BOTH;
		limitesHeader.anchor = GridBagConstraints.NORTH;
		limitesHeader.insets = new Insets(0, 0, 10, 0);
		
		limitesNome = new GridBagConstraints();
		limitesNome.gridx = 0;
		limitesNome.gridy = 4;
		limitesNome.gridwidth = 5;
		limitesNome.weightx = 1.0;
		limitesNome.anchor = GridBagConstraints.WEST;
		
		limitesMesa = new GridBagConstraints();
		limitesMesa.gridx = 0;
		limitesMesa.gridy = limitesNome.gridy + 1;
		limitesMesa.gridwidth = 5;
		limitesMesa.weightx = 1.0;
		limitesMesa.anchor = GridBagConstraints.WEST;
		
		limitesFormaDePagamento = new GridBagConstraints();
		limitesFormaDePagamento.gridx = 0;
		limitesFormaDePagamento.gridy = limitesNome.gridy + 2;
		limitesFormaDePagamento.gridwidth = 5;
		limitesFormaDePagamento.weightx = 1.0;
		limitesFormaDePagamento.anchor = GridBagConstraints.WEST;
		
		limitesProdutos = new GridBagConstraints();
		limitesProdutos.gridx = 0;
		limitesProdutos.gridy = limitesNome.gridy + 3;
		limitesProdutos.gridwidth = 5;
		limitesProdutos.weightx = 1.0;
		limitesProdutos.anchor = GridBagConstraints.WEST;
		
		limitesBotoes = new GridBagConstraints();
		limitesBotoes.gridx = 0;
		limitesBotoes.gridy = limitesNome.gridy + 4;
		limitesBotoes.gridwidth = 5;
		limitesBotoes.weightx = 1.0;
		limitesBotoes.anchor = GridBagConstraints.SOUTH;
		
		//Configurar Labels
		mesaLabel = new JLabel("Mesa: ", JLabel.LEFT);
		formaDePagamentoLabel = new JLabel("Forma de pagamento: ", JLabel.LEFT);
		
		//Configurar Campos de texto
		nome.setColumns(21);
		
		mesa = new JFormattedTextField(mesaMask);
		mesa.setColumns(8);
		
		//ConfigurarComboBox
		formaDePagamento = new JComboBox<String>();
		formaDePagamento.addItem("Dinheiro");
		formaDePagamento.addItem("Cartão de Crédito");
		
		//Configurar botões
		adicionarProduto = new JButton("Adicionar Produto");
		adicionarProduto.setActionCommand("adicionar produto");
		
		removerProduto = new JButton("Remover produto");
		removerProduto.setActionCommand("remover produto");
		
		listener = new CadastrarPedidoC().new ButtonClickListener(vendedor, nome, mesa, 
						formaDePagamento, produtosBg, this.bgPrincipal, menuProduto);
		
		adicionarProduto.addActionListener(listener);
		removerProduto.addActionListener(listener);
		
		setComumActions(listener);
	}
	private void adicionarNoBg(){
		bgPrincipal.add(headerBg, limitesHeader);
		bgPrincipal.add(nomeBg, limitesNome);
		bgPrincipal.add(mesaBg, limitesMesa);
		bgPrincipal.add(produtosBgScroll, limitesProdutos);
		bgPrincipal.add(formaDePagamentoBg, limitesFormaDePagamento);
		bgPrincipal.add(botoesBg, limitesBotoes);
		
		headerBg.add(headerLabel);
		
		nomeBg.add(nomeLabel);
		nomeBg.add(nome);
		
		mesaBg.add(mesaLabel);
		mesaBg.add(mesa);

		
		produtosBg.add(menuProduto.get());
		
		formaDePagamentoBg.add(formaDePagamentoLabel);
		formaDePagamentoBg.add(formaDePagamento);
		
		botoesBg.add(confirmar);
		botoesBg.add(cancelar);
		botoesBg.add(adicionarProduto);
		botoesBg.add(removerProduto);
	}
	
	public void inserir(MainFrameV mainFrame) {
		mainFrame.getMainFrame().add(bgPrincipal, limitesExternos);
		adicionarNoBg();
	}
	
	public JPanel get() {
		adicionarNoBg();
		return bgPrincipal;
	}
	
	public JPanel getProdutosBg() {
		return produtosBg;
	}

	public SelecionarProdutoV getMenuProduto() {
		return menuProduto;
	}
	
	public CadastrarPedidoC.ButtonClickListener getListener() {
		return listener;
	}
}

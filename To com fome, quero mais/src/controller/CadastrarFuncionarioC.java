package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import model.FuncionarioM;
import model.LoginM;

public class CadastrarFuncionarioC extends LoginAndCadastroC{
	protected JPasswordField senha;
	protected JPasswordField senhaAdm;
	protected JPanel login;
	
	protected String menuAnterior;

	public CadastrarFuncionarioC(){};
	
	public CadastrarFuncionarioC(JFormattedTextField nome, JPasswordField senha, 
			JPasswordField senhaAdm, JPanel bgPrincipal) {
		
		super(nome, bgPrincipal);
		this.senha = senha;
		this.senhaAdm = senhaAdm;
	}

	public class ButtonClickListener extends CadastrarFuncionarioC implements ActionListener{
		
		public ButtonClickListener(JFormattedTextField nome, JPasswordField senha, 
				JPasswordField senhaAdm, JPanel bgPrincipal) {
			
			super(nome, senha, senhaAdm, bgPrincipal);
		}

		public void actionPerformed(ActionEvent e) {
			String comando = e.getActionCommand();
			
			if(comando.equals("confirmar")){
				
				if(nome.equals("                                 ") || nome.getValue() == null || 
				   senha.getPassword().length == 0                 || senhaAdm.getPassword().length == 0){
					
					JOptionPane.showMessageDialog(null, "Há campos em branco!", "Campos em branco!", JOptionPane.ERROR_MESSAGE);
				}
				else if (new LoginM().getFuncionarios().contains(nome.getText())){
					JOptionPane.showMessageDialog(null, "Usuário já cadastrado", "Ação inválida", JOptionPane.ERROR_MESSAGE);
				}
				else{
					if(new FuncionarioM().validaSenhaAdm(senhaAdm)){
						
						FuncionarioM funcionario = new FuncionarioM(nome.getText(), senha.getPassword());
						JOptionPane.showMessageDialog(null, "Funcionario cadastrado com sucesso!", "OK!", JOptionPane.INFORMATION_MESSAGE);
						
						bgPrincipal.setVisible(false);
						funcionario.cadastra();
						
						nome.setText("");
						senha.setText("");
						senhaAdm.setText("");
						
						login.setVisible(true);
					}
					else{
						JOptionPane.showMessageDialog(null, "Senha do administrador incorreta!", "Dados inválidos!", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
			else if(comando.equals("cancelar")){
				
				bgPrincipal.setVisible(false);
				
				if(menuAnterior.equals("login")){
					login.setVisible(true);
				}
				else if(menuAnterior.equals("menu principal")){
					menuPrincipal.get().setVisible(true);
				}
				
			}
			else{
				/*NOTHING*/
			}
		}
	}
	
	public String getMenuAnterior() {
		return menuAnterior;
	}
	
	public void setMenuAnterior(String menuAnterior) {
		this.menuAnterior = menuAnterior;
	}
	
	public JPanel getLogin() {
		return login;
	}

	public void setLogin(JPanel login) {
		this.login = login;
	}

}

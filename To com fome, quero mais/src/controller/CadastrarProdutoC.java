package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.ProdutoM;

public class CadastrarProdutoC extends LoginAndCadastroC{
	protected JFormattedTextField preco;
	protected JFormattedTextField quantidade;
	protected JFormattedTextField quantidadeMin;

	public CadastrarProdutoC(){};
	
	public CadastrarProdutoC(JFormattedTextField nome, JFormattedTextField preco, 
			JFormattedTextField quantidade, JFormattedTextField quantidadeMin, JPanel bgPrincipal) {
		
		super(nome, bgPrincipal);
		this.preco = preco;
		this.quantidade = quantidade;
		this.quantidadeMin = quantidadeMin;
	}
	
	public class ButtonClickListener extends CadastrarProdutoC implements ActionListener{
		
		public ButtonClickListener(JFormattedTextField nome, JFormattedTextField preco, 
				JFormattedTextField quantidade, JFormattedTextField quantidadeMin, JPanel bgPrincipal) {
			
			super(nome, preco, quantidade, quantidadeMin, bgPrincipal);
		}

		public void actionPerformed(ActionEvent e) {
			String comando = e.getActionCommand();
			String precoString;
			
			/* evitar excessão null pointer */
			if(preco.getText().equals("R$000,00")) 
				precoString = "00000"; 
			else
				precoString = preco.getValue().toString();
			/*                              */
			
			
			if(comando.equals("confirmar")){
				
				if(nome.getText().toString().equals("                              ") || nome.getValue() == null ||
					preco.getValue().toString().equals("00000")            || preco.getValue() == null ||
					quantidade.getText().toString().equals("000")         || quantidade.getText() == null){
					
					JOptionPane.showMessageDialog(null, "Nome, preço e quantidade devem estar preechidos!", "Campos Vazios!", JOptionPane.ERROR_MESSAGE);
				}
				else if(quantidadeMin.getValue() == null){
					quantidadeMin.setValue("000");
				}
				else{
					ProdutoM produto = new ProdutoM(nome.getText(), precoString, quantidade.getText(), 
					quantidadeMin.getText());
					produto.cadastra();
					
					JOptionPane.showMessageDialog(null, "Produto cadastrado com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
					
					bgPrincipal.setVisible(false);
					menuPrincipal.get().setVisible(true);
				}
			}			
			else if(comando.equals("cancelar")){
				
				bgPrincipal.setVisible(false);
				
				nome.setText("");
				preco.setText("00000");
				quantidade.setText("000");
				quantidadeMin.setText("000");
				
				menuPrincipal.get().setVisible(true);
			}
			else{
				/*NOTHING*/
			}
		}
		
	}
}

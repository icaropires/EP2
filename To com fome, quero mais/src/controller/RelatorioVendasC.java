package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

public class RelatorioVendasC {
	protected JPanel bgPrincipal;
	protected MenuPrincipalC menuPrincipal;

	public RelatorioVendasC(){};
	
	public RelatorioVendasC(JPanel bgPrincipal) {
		this.bgPrincipal = bgPrincipal;
	}
	
	public class ButtonClickListener extends RelatorioVendasC implements ActionListener{
		
		public ButtonClickListener(JPanel bgPrincipal) {
			
			super(bgPrincipal);
		}

		public void actionPerformed(ActionEvent e) {
			String comando = e.getActionCommand();
			
			if(comando.equals("ok")){
				bgPrincipal.setVisible(false);
				menuPrincipal.bgPrincipal.setVisible(true);
			}
			else{
				/*NOTHING*/
			}
		}
	}

	public void setMenuPrincipal(MenuPrincipalC menuPrincipal) {
		this.menuPrincipal = menuPrincipal;
	}
}

package controller;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.ProdutoM;
import model.VendaM;
import view.SelecionarProdutoV;

public class CadastrarPedidoC extends LoginAndCadastroC{
	protected JFormattedTextField mesa;
	protected JFormattedTextField observacoes;
	protected JComboBox<String> formaDePagamento;
	protected ArrayList<SelecionarProdutoV> menusProdutos = new ArrayList<SelecionarProdutoV>();
	protected JPanel produtosBg;
	protected String vendedor;
	protected SelecionarProdutoV menuProduto;
	protected ProdutoM novoEstoque;
	
	public CadastrarPedidoC() {}
	
	public CadastrarPedidoC(String vendedor, JFormattedTextField nome, JFormattedTextField mesa, 
			JComboBox<String> formaDePagamento, JPanel produtosBg, JPanel bgPrincipal, SelecionarProdutoV menuProduto) {
		
		super(nome, bgPrincipal);
		this.menuProduto = menuProduto;
		this.vendedor = vendedor;
		this.mesa = mesa;
		this.formaDePagamento = formaDePagamento;
		this.produtosBg = produtosBg;
		menusProdutos.add(menuProduto);
	}

	public class ButtonClickListener extends CadastrarPedidoC implements ActionListener{ 
		public ButtonClickListener(String vendedor, JFormattedTextField nome, JFormattedTextField mesa, 
				JComboBox<String> formaDePagamento, JPanel produtosBg, JPanel bgPrincipal,  SelecionarProdutoV menuProduto){
			
			super(vendedor, nome, mesa,formaDePagamento, produtosBg, bgPrincipal, menuProduto);
		}

		public void actionPerformed(ActionEvent e) {
			String comando = e.getActionCommand();
			
			if(comando.equals("confirmar")){
				boolean produtosIguais = false;
				
				for(int i = 0; i < menusProdutos.size(); i++){
					for(int j = i+1; j < menusProdutos.size(); j++){
						if(menusProdutos.get(i).getProdutoBox().getSelectedItem().equals( menusProdutos.get(j).getProdutoBox().getSelectedItem() )){
							produtosIguais = true;
						}
					}	
				}
				
				if(nome.getText().toString().equals("                              ") || nome.getValue() == null || 
				   mesa.getText().toString().equals("00")){
					
					JOptionPane.showMessageDialog(null, "Nome e mesa devem estar preenchidos!", "Campos vazios!!!",JOptionPane.ERROR_MESSAGE);
				}
				else if(menusProdutos.get(0).isSemEstoque()){
					JOptionPane.showMessageDialog(null, "Não há produtos disponíveis em estoque", "Sem estoque!", JOptionPane.ERROR_MESSAGE);
				}
				else if(produtosIguais){
					JOptionPane.showMessageDialog(null, "Selecione apenas uma vez cada produto", "Não permitido", JOptionPane.ERROR_MESSAGE);
				}
				else{
					
					novoEstoque.leiaArquivo();
					novoEstoque.atualizaNovos();
					
					VendaM novaVenda = new VendaM(this);
					int novaQuantidade;
					int indiceProdutoAtual;
					
					novaVenda.cadastra();
					
					//Pós venda
					for(int i = 0; i < menusProdutos.size(); i++){
						novaQuantidade = novoEstoque.descontarQuantidade(menusProdutos.get(i).getProdutoBox().getSelectedItem().toString(), 
														   menusProdutos.get(i).getQuantidadeBox().getSelectedItem().toString());
						
						indiceProdutoAtual = novoEstoque.getProdutos().indexOf(menusProdutos.get(i).getProdutoBox().getSelectedItem().toString());
								
						if(novaQuantidade <=  novoEstoque.getQuantidadesMin().get(indiceProdutoAtual) || novaQuantidade == 0){
							JOptionPane.showMessageDialog(null, menusProdutos.get(i).getProdutoBox().getSelectedItem().toString().trim() + 
								" está abaixo da quantidade mínima e não será mais vendido até que o estoque seja reposto", "Alerta!", JOptionPane.WARNING_MESSAGE);
							
						}
					}
					
					new File("./defaults/produtos").delete();
					novoEstoque.cadastra();
					
					JOptionPane.showMessageDialog(null, "Venda registrada, faça uma nova!", "OK!", JOptionPane.INFORMATION_MESSAGE);
					
					limpaCampos();
				}
				
			}
			else if(comando.equals("cancelar")){
				
				bgPrincipal.setVisible(false);
				limpaCampos();
				menuPrincipal.get().setVisible(true);
				
			}
			else if(comando.equals("adicionar produto")){
				produtosBg.revalidate();
				menusProdutos.add(new SelecionarProdutoV());
				produtosBg.setPreferredSize(new Dimension(800, 120*menusProdutos.size())); 
				
				produtosBg.add(menusProdutos.get(menusProdutos.size()-1).get());
			}
			else if(comando.equals("remover produto")){
				
				if(menusProdutos.size() > 1){
					produtosBg.revalidate();
					produtosBg.remove(menusProdutos.get(menusProdutos.size()-1).get());
					produtosBg.setPreferredSize(new Dimension(800, 120*menusProdutos.size()));
					menusProdutos.remove(menusProdutos.size()-1);
				}
				else{
					
					JOptionPane.showMessageDialog(null, "Já está no valor mínimo de produtos!",
							                      "Não será possível excluir", JOptionPane.ERROR_MESSAGE);
				}
			}
			else{
				/*NOTHING*/
			}
		}
		
		public void limpaCampos(){
			nome.setValue(null);
			mesa.setText("00");
			
			for(int i = menusProdutos.size()-1; i >= 0; i--){
				
				produtosBg.remove(menusProdutos.get(i).get());
				menusProdutos.remove(i);
			}
			
			menusProdutos.add(new SelecionarProdutoV());
			produtosBg.add(menusProdutos.get(0).get());
			menusProdutos.get(0).atualizarProdutoComboBox(new ProdutoM());
			
			produtosBg.revalidate();
			produtosBg.repaint();
		}
	}
	
	public void setNovoEstoque(ProdutoM novoEstoque) {
		this.novoEstoque = novoEstoque;
	}

	public JFormattedTextField getMesa() {
		return mesa;
	}

	public JComboBox<String> getFormaDePagamento() {
		return formaDePagamento;
	}

	public ArrayList<SelecionarProdutoV> getMenusProdutos() {
		return menusProdutos;
	}

	public String getVendedor() {
		return vendedor;
	}
}

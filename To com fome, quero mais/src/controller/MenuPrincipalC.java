package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.ProdutoM;
import view.CadastrarFuncionarioV;
import view.CadastrarPedidoV;
import view.CadastrarProdutoV;
import view.MainFrameV;
import view.MenuPrincipalV;
import view.ModificarProdutoV;
import view.RelatorioVendasV;
import view.SelecionarProdutoV;

public class MenuPrincipalC {
	protected String vendedor;
	protected JPanel bgPrincipal;

	protected MainFrameV mainFrame;
	protected CadastrarPedidoV cadastrarPedido;
	protected CadastrarProdutoV cadastrarProduto;
	protected CadastrarFuncionarioV cadastrarFuncionario;
	protected MenuPrincipalV menuPrincipal;
	protected RelatorioVendasV relatorioVendas;
	protected ModificarProdutoV modificarProduto;
	
	protected ProdutoM produtosCadastrados = new ProdutoM();
	protected JPanel login;

	public MenuPrincipalC(){
	}
	
	public class ButtonClickListener extends MenuPrincipalC implements ActionListener{
		
		public ButtonClickListener(JPanel bgPrincipal){
			this.bgPrincipal = bgPrincipal;
		}
	
		public void actionPerformed(ActionEvent e) {
			String comando = e.getActionCommand();
			
			if(comando.equals("cadastrar pedido")){
				
				if ( new ProdutoM().getProdutos().size() == 0){
					JOptionPane.showMessageDialog(null, "Não há produtos cadastrados! Primeiro cadastre um produto", "Sem Produtos!", JOptionPane.ERROR_MESSAGE);
				}
				
				else{
					cadastrarPedido = new CadastrarPedidoV(vendedor);
					bgPrincipal.setVisible(false);
					
					cadastrarPedido.inserir(mainFrame);
					cadastrarPedido.getListener().setNovoEstoque(produtosCadastrados);
					cadastrarPedido.getListener().setMainFrame(mainFrame);
					cadastrarPedido.getMenuProduto().atualizarProdutoComboBox(cadastrarPedido.getMenuProduto().getProdutosCadastrados());
					cadastrarPedido.getListener().setMenuPrincipal(menuPrincipal);
					
					cadastrarPedido.get().setVisible(true);
				}
			}
			else if(comando.equals("cadastrar produto")){
				
				cadastrarProduto = new CadastrarProdutoV();
				bgPrincipal.setVisible(false);
				
				cadastrarProduto.inserir(mainFrame);
				cadastrarProduto.getListener().setMainFrame(mainFrame);
				cadastrarProduto.getListener().setMenuPrincipal(menuPrincipal);
				
				cadastrarProduto.get().setVisible(true);
				
			}
			else if(comando.equals("cadastrar funcionario")){
				
				bgPrincipal.setVisible(false);
				
				cadastrarFuncionario.inserir(mainFrame);
				cadastrarFuncionario.getListener().setMainFrame(mainFrame);
				cadastrarFuncionario.getListener().setMenuPrincipal(menuPrincipal);
				cadastrarFuncionario.getListener().setLogin(login);
				cadastrarFuncionario.getListener().setMenuAnterior("menu principal");
				
				cadastrarFuncionario.get().setVisible(true);
				
			}
			else if(comando.equals("trocar user")){
				bgPrincipal.setVisible(false);
				
				login.setVisible(true);
			}
			else if(comando.equals("relatorio de vendas")){
				bgPrincipal.setVisible(false);
				
				relatorioVendas = new RelatorioVendasV();
				relatorioVendas.inserir(mainFrame);
				relatorioVendas.getListener().setMenuPrincipal(this);
				
				relatorioVendas.atualizarTexto();
				
				relatorioVendas.get().setVisible(true);
			}
			else if(comando.equals("modificar produto")){
				bgPrincipal.setVisible(false);
				
				
				modificarProduto = new ModificarProdutoV();
				new SelecionarProdutoV().atualizarProdutoComboBox(produtosCadastrados);
				modificarProduto.getListener().setProdutosCadastrados(produtosCadastrados);
				modificarProduto.inserir(mainFrame);
				modificarProduto.getListener().setMenuPrincipal(this);
				
				modificarProduto.get().setVisible(true);
				
				JOptionPane.showMessageDialog(null, "Caso deseje alterar preço, quantidade ou quantidade mínima, preencha o campo desejado até o final", "Atenção!", JOptionPane.WARNING_MESSAGE);
			}
		}
	}
	
	
	public void setMenuPrincipal(MenuPrincipalV menuPrincipal) {
		this.menuPrincipal = menuPrincipal;
	}
	
	public void setMainFrame(MainFrameV mainFrame) {
		this.mainFrame = mainFrame;
	}
	
	public void setCadastrarFuncionario(CadastrarFuncionarioV cadastrarFuncionario) {
		this.cadastrarFuncionario = cadastrarFuncionario;
	}

	public String getVendedor() {
		return vendedor;
	}
	
	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}
	
	public void setLogin(JPanel login) {
		this.login = login;
	}
}

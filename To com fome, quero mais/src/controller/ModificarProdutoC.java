package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.ProdutoM;
import view.SelecionarProdutoV;

public class ModificarProdutoC extends LoginAndCadastroC{
	protected JFormattedTextField preco;
	protected JFormattedTextField quantidade;
	protected JFormattedTextField quantidadeMin;
	protected ProdutoM produtosCadastrados;
	protected MenuPrincipalC menuPrincipal;
	protected JComboBox<String> produto;
	
	public ModificarProdutoC(){};
	
	public ModificarProdutoC(JFormattedTextField nome, JFormattedTextField preco, 
			JFormattedTextField quantidade, JFormattedTextField quantidadeMin, JComboBox<String> produto, JPanel bgPrincipal) {
		
		super(nome, bgPrincipal);
		this.preco = preco;
		this.quantidade = quantidade;
		this.quantidadeMin = quantidadeMin;
		this.produto = produto;
	}
	
	public class ButtonClickListener extends ModificarProdutoC implements ActionListener{
		
		public ButtonClickListener(JFormattedTextField nome, JFormattedTextField preco, 
				JFormattedTextField quantidade, JFormattedTextField quantidadeMin, JComboBox<String> produto, JPanel bgPrincipal) {
			
			super(nome, preco, quantidade, quantidadeMin, produto, bgPrincipal);
			
		}

		public void actionPerformed(ActionEvent e) {
			String comando = e.getActionCommand();
			
			if(comando.equals("confirmar")){
				new SelecionarProdutoV().atualizarProdutoComboBoxOcultos(produtosCadastrados);
				produtosCadastrados.leiaArquivo();
				produtosCadastrados.atualizaNovos();
				String precoString;
				
				int indice = produtosCadastrados.getProdutos().indexOf(produto.getSelectedItem());
				
				
				if(nome.getValue() == null || nome.getText().equals("                              "))
					nome.setText(produtosCadastrados.getProdutos().get(indice));
				
				if(preco.getValue() == null || preco.getValue().toString().equals("_____")){
					indice = produtosCadastrados.getProdutos().indexOf(produto.getSelectedItem());
					precoString = produtosCadastrados.getPrecosNovos().get(indice).toString();
					preco.setValue(precoString);
				}
				else{
					precoString = Double.toString((Double.parseDouble(preco.getValue().toString())/100));
				}
				
				if(quantidade.getValue() == null || quantidade.getValue().toString().equals("___"))
					quantidade.setValue(produtosCadastrados.getQuantidades().get(indice).toString());
					
				if(quantidadeMin.getValue() == null || quantidadeMin.getValue().toString().equals("___"))	
					quantidadeMin.setValue(produtosCadastrados.getQuantidadesMin().get(indice).toString());
				
				produtosCadastrados.alterar(indice , nome.getText(), precoString, quantidade.getValue().toString(),
						quantidadeMin.getValue().toString());
				
				new File("./defaults/produtos").delete();
				produtosCadastrados.cadastra();
				
				JOptionPane.showMessageDialog(null, "Produto alterado com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
				
				bgPrincipal.setVisible(false);
				menuPrincipal.bgPrincipal.setVisible(true);
			}			
			else if(comando.equals("cancelar")){
				
				bgPrincipal.setVisible(false);
				
				nome.setText("");
				produto.setSelectedIndex(0);
				preco.setText("00000");
				quantidade.setText("000");
				quantidadeMin.setText("000");
				new SelecionarProdutoV().atualizarProdutoComboBoxOcultos(produtosCadastrados);
				
				menuPrincipal.bgPrincipal.setVisible(true);
			}
			else if(comando.equals("excluir produto")){
				new SelecionarProdutoV().atualizarProdutoComboBoxOcultos(produtosCadastrados);
				produtosCadastrados = new ProdutoM();
				
				int indice = produtosCadastrados.getProdutos().indexOf(produto.getSelectedItem());
				
				produtosCadastrados.getProdutosNovos().remove(indice);
				produtosCadastrados.getQuantidadesNovos().remove(indice);
				produtosCadastrados.getQuantidadesMinNovos().remove(indice);
				produtosCadastrados.getPrecosNovos().remove(indice);
				
				new File("./defaults/produtos").delete();
				produtosCadastrados.cadastra();
				
				JOptionPane.showMessageDialog(null, "Produto excluído com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
				
				bgPrincipal.setVisible(false);
				menuPrincipal.bgPrincipal.setVisible(true);
			}
			else{
				/*NOTHING*/
			}
		}
	}
	
	public void setProdutosCadastrados(ProdutoM produtosCadastrados) {
		this.produtosCadastrados = produtosCadastrados;
	}

	public void setMenuPrincipal(MenuPrincipalC menuPrincipal) {
		this.menuPrincipal = menuPrincipal;
	}
	
}

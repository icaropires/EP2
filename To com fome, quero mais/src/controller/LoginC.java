package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import model.LoginM;
import view.CadastrarFuncionarioV;
import view.MainFrameV;
import view.MenuPrincipalV;

public class LoginC extends LoginAndCadastroC{
	protected JPasswordField senha;
	protected CadastrarFuncionarioV cadastrarFuncionario;
	
	public LoginC(){};
	
	public LoginC(JFormattedTextField nome, JPasswordField senha, JPanel bgPrincipal, MainFrameV mainFrame) {
		super(nome, bgPrincipal);
		this.senha = senha;
		this.mainFrame = mainFrame;
		
		cadastrarFuncionario = new CadastrarFuncionarioV();
		menuPrincipal = new MenuPrincipalV();
	}
	
	public class ButtonClickListener extends LoginC implements ActionListener{
		
		public ButtonClickListener(JFormattedTextField nome, JPasswordField senha, JPanel bgPrincipal, MainFrameV mainFrame) {
			super(nome, senha, bgPrincipal, mainFrame);
		}

		public void actionPerformed(ActionEvent e) {
			String comando = e.getActionCommand();
			
			if(comando.equals("confirmar")){
				LoginM login = new LoginM(nome.getText(), senha.getPassword());
				
				if(login.valida()){
					
					if(nome .equals("                                 ") || nome.getValue() == null || 
					   senha.getPassword().length == 0){
						
						JOptionPane.showMessageDialog(null, "Há campos em branco!", "Campos em branco!", JOptionPane.ERROR_MESSAGE);
					}
					
					bgPrincipal.setVisible(false);
					
					menuPrincipal.inserir(mainFrame);
					menuPrincipal.getListener().setMainFrame(mainFrame);
					menuPrincipal.getListener().setVendedor(nome.getText());
					menuPrincipal.getListener().setCadastrarFuncionario(cadastrarFuncionario);
					menuPrincipal.getListener().setMenuPrincipal(menuPrincipal);
					menuPrincipal.getListener().setLogin(bgPrincipal);
					
					nome.setText("                              ");
					senha.setText("");
					
					menuPrincipal.get().setVisible(true);
					ok = true;
				}
				else{
					JOptionPane.showMessageDialog(null, "Ou senha ou usuário incorreto!", "Dados inválidos", JOptionPane.ERROR_MESSAGE);
				}
				
			}
			else if(comando.equals("cadastrar funcionario")){
				bgPrincipal.setVisible(false);
				
				nome.setText("");
				senha.setText("");
				
				cadastrarFuncionario.getListener().setMenuAnterior("login");
				cadastrarFuncionario.getListener().setLogin(bgPrincipal);
				cadastrarFuncionario.getListener().setMainFrame(mainFrame);
				cadastrarFuncionario.inserir(mainFrame);
				
				cadastrarFuncionario.get().setVisible(true);
			}
			else{
				/*NOTHING*/
			}
		}
	}
}

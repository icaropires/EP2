package controller;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.ProdutoM;
import view.SelecionarProdutoV;

public class SelecionarProdutoC {
	protected SelecionarProdutoV selecionarProdutoMenu;
	protected JPanel quantidadeBg;
	protected ProdutoM produtosCadastrados;
	protected JComboBox<Integer> quantidadeComboBox;
	protected JLabel quantidadeMaxLabel;
	protected JLabel precoInfoLabel;
	protected JLabel quantidadeMinLabel;
	protected JComboBox<String> produtoBox;
	protected int quantidadeDoSelecionado;
	protected double precoDoSelecionado;
	protected int quantidadeMinSelecionado;
	
	public SelecionarProdutoC(){}
	
	public SelecionarProdutoC(SelecionarProdutoV selecionarProdutoMenu){
		this.selecionarProdutoMenu = selecionarProdutoMenu;
		produtoBox = selecionarProdutoMenu.getProdutoBox();
		quantidadeBg = selecionarProdutoMenu.getQuantidadeBg();
		produtosCadastrados = selecionarProdutoMenu.getProdutosCadastrados();
		quantidadeComboBox = selecionarProdutoMenu.getQuantidadeBox();
		quantidadeMaxLabel = selecionarProdutoMenu.getQuantidadeMaxLabel();
		precoInfoLabel = selecionarProdutoMenu.getPrecoInfoLabel();
		quantidadeMinLabel = selecionarProdutoMenu.getQuantidadeMinLabel();
	}
	
	public class ComboBoxListener extends SelecionarProdutoC implements ItemListener{

		public ComboBoxListener(SelecionarProdutoV selecionarProdutoMenu){
			super(selecionarProdutoMenu);
		}
		
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED ) {
				
				quantidadeDoSelecionado = produtosCadastrados.getQuantidades().get(produtosCadastrados.getProdutos().indexOf( ( ( (produtoBox.getSelectedItem()) ) ) ) );
				precoDoSelecionado = produtosCadastrados.getPrecos().get(produtosCadastrados.getProdutos().indexOf( ( ( (produtoBox.getSelectedItem()) ) ) ) );
				quantidadeMinSelecionado = produtosCadastrados.getQuantidadesMin().get(produtosCadastrados.getProdutos().indexOf(produtoBox.getSelectedItem()));
				
				quantidadeMaxLabel.setText("Em estoque: " + quantidadeDoSelecionado);
				precoInfoLabel.setText("Preco: " + precoDoSelecionado);
				quantidadeMinLabel.setText("Mínimo: " + quantidadeMinSelecionado);
				
				quantidadeBg.revalidate();
				
				quantidadeComboBox.removeAllItems();
				for(int i = 1; i <= quantidadeDoSelecionado; i++){
					quantidadeComboBox.addItem(i);
				}
		    }
		}
	}
}

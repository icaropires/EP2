package controller;

import javax.swing.JFormattedTextField;
import javax.swing.JPanel;

import view.MainFrameV;
import view.MenuPrincipalV;

public abstract class LoginAndCadastroC{
	protected JFormattedTextField nome;
	protected JPanel bgPrincipal;
	protected boolean ok;
	
	protected MainFrameV mainFrame;
	protected MenuPrincipalV menuPrincipal;
	
	public LoginAndCadastroC(){};
	
	public LoginAndCadastroC(JFormattedTextField nome, JPanel bgPrincipal){
		this.nome = nome;
		this.bgPrincipal = bgPrincipal;
	}
	
	public void setMenuPrincipal(MenuPrincipalV menuPrincipal) {
		this.menuPrincipal = menuPrincipal;
	}

	public void setMainFrame(MainFrameV mainFrame) {
		this.mainFrame = mainFrame;
	}

	public JFormattedTextField getNome() {
		return nome;
	}
	
	public boolean isOk() {
		return ok;
	}
}

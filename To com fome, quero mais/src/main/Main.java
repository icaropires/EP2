/*Sufixo nos nomes das classes:
M - Model; V - View; C - Controller*/

package main;

import view.MainFrameV;
import view.LoginV;

public class Main {
	public static void main(String[] args) {
		MainFrameV mainFrame = new MainFrameV();
		LoginV loginV = new LoginV(mainFrame);
		
		loginV.inserir(mainFrame);
		
		mainFrame.getMainFrame().setVisible(true);
	}
}
